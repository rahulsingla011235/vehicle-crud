const CONSTANTS = require('../utils/constants');
const { userService } = require('../services')
const commonFunctions = require('./utils');
const CONFIG = require('../../config')

let dbUtils = {};

/**
 * function to check valid reference from models.
 */
dbUtils.checkValidReference = async (document, referenceMapping) => {
  for (let key in referenceMapping) {
    let model = referenceMapping[key];
    if (!!document[key] && !(await model.findById(document[key]))) {
      throw CONSTANTS.RESPONSE.ERROR.BAD_REQUEST(key + ' is invalid.');
    }
  }
};

dbUtils.addDefaltUser = async () => {
  let user = await userService.getUser({});
  if(!user){
    await userService.saveUser({email: CONFIG.defaultUserEmail, password: commonFunctions.hashPassword(CONFIG.defaultUserPassword) });
  }
};

module.exports = dbUtils;