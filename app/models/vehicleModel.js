"use strict";
/************* Modules ***********/
const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;
/**************************************************
 ************* Vehicle Model or collection ***********
 **************************************************/
const vehicleSchema = new Schema({
    make: { type: String },
    model: { type: String },
    year: { type: String },
    isDeleted: { type: Boolean, default: false }
}, {'timestamps': true});

module.exports = MONGOOSE.model('vehicle', vehicleSchema);