"use strict";
/************* Modules ***********/
const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;
/**************************************************
 ************* User Model or collection ***********
 **************************************************/
const userSchema = new Schema({
    email: { type: String },
    password: { type: String },
}, {'timestamps': true});

module.exports = MONGOOSE.model('user', userSchema);