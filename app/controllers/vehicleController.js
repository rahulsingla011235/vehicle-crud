"use strict";
const HELPERS = require("../helpers");
const { MESSAGES, ERROR_TYPES, NORMAL_PROJECTION, PAGINATION } = require('../utils/constants');
const SERVICES = require('../services');

let vehicleController = {};

/**
 * function to add new vehicle
 * @param {*} payload 
 * @returns 
 */
vehicleController.addVehicle = async (payload) => {
  let isVehicleAlreadyExists = await SERVICES.vehicleService.getVehicle({ make: payload.make, model: payload.model, year: payload.year, isDeleted: false });
  if (!isVehicleAlreadyExists) {
    let newVehicle = await SERVICES.vehicleService.addVehicle(payload);

    return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.VEHICLE_ADDED_SUCCESSFULLY), { user: newVehicle });
  }
  return HELPERS.responseHelper.createErrorResponse(MESSAGES.VEHICLE_ALREADY_EXISTS, ERROR_TYPES.ALREADY_EXISTS);  
};

/**
 * function to update vehicle
 * @param {*} payload 
 * @returns 
 */
vehicleController.updateVehicle = async (payload) => {
  let vehicle = await SERVICES.vehicleService.getVehicle({ _id: payload.id, isDeleted: false});
  if(vehicle)
  {
    let dataToUpdate = {make: payload.make?payload.make:vehicle.make, model: payload.model?payload.model:vehicle.model, year: payload.year?payload.year:vehicle.year}
    let isVehicleExists = await SERVICES.vehicleService.getVehicle({...dataToUpdate, isDeleted: false});
    if(isVehicleExists)
    {
      return HELPERS.responseHelper.createErrorResponse(MESSAGES.VEHICLE_ALREADY_EXISTS, ERROR_TYPES.ALREADY_EXISTS);  
    }
    let updatedVehicle = await SERVICES.vehicleService.updateVehicle({ _id: payload.id }, dataToUpdate, {new: true, projection: NORMAL_PROJECTION});

    return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.VEHICLE_UPDATE_SUCCESSFULLY), { vehicle: updatedVehicle });
  }
  return HELPERS.responseHelper.createErrorResponse(MESSAGES.NOT_FOUND, ERROR_TYPES.DATA_NOT_FOUND);  
};

/**
 * function to delete vehicle
 * @param {*} payload 
 * @returns 
 */
vehicleController.deleteVehicle = async (payload) => {
  let vehicle = await SERVICES.vehicleService.getVehicle({ _id: payload.id , isDeleted: false});
  if(vehicle)
  {
    let updatedVehicle = await SERVICES.vehicleService.updateVehicle({ _id: payload.id }, {isDeleted: true});

    return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.VEHICLE_DELETED_SUCCESSFULLY));
  }
  return HELPERS.responseHelper.createErrorResponse(MESSAGES.NOT_FOUND, ERROR_TYPES.DATA_NOT_FOUND); 
};

/**
 * function to get single vehicle
 * @param {*} payload 
 * @returns 
 */
vehicleController.getVehicle = async (payload) => {
  let vehicle = await SERVICES.vehicleService.getVehicle({ _id: payload.id, isDeleted: false});
  if (vehicle) {
    return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.VEHICLE_FETCHED_SUCCESSFULLY), { vehicle });
  }
  return HELPERS.responseHelper.createErrorResponse(MESSAGES.NOT_FOUND, ERROR_TYPES.DATA_NOT_FOUND);
};

/**
 * function to fetch vehicles
 * @param {*} payload 
 * @returns 
 */
vehicleController.getVehicles = async (payload) => {
  let limit = payload.limit || PAGINATION.DEFAULT_LIMIT;
  let skipDocuments = ((payload.pageNumber || PAGINATION.DEFAULT_PAGE_NUMBER) - 1 )*limit;
  let options = { skip: skipDocuments, limit , lean: true };

  let vehicles = await SERVICES.vehicleService.getVehicles({isDeleted: false}, NORMAL_PROJECTION ,options);
  if (vehicles) {
    return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.VEHICLE_FETCHED_SUCCESSFULLY), { vehicles });
  }
  return HELPERS.responseHelper.createErrorResponse(MESSAGES.NOT_FOUND, ERROR_TYPES.DATA_NOT_FOUND);
};

module.exports = vehicleController;