'use strict';
const { userModel} = require('../models');
const utils = require('../utils/utils');

let userService = {};

/*
 * function to save user 
 */
userService.saveUser = async (criteria) => {
  return await userModel(criteria).save();
};

/**
 * function to get user 
 */
userService.getUser = async (criteria) => {
  return await userModel.findOne(criteria).lean();
};

module.exports = userService;