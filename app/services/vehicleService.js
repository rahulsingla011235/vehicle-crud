'use strict';
const { vehicleModel } = require('../models');

let vehicleService = {};

/**
 * function to add vehicle 
 */
vehicleService.addVehicle = async (payload) => {
  return await vehicleModel(payload).save();
};

/**
 * function to update vehicle 
 */
vehicleService.updateVehicle = async (criteria, dataToUpdate, options ={new: true}) => {
  return await vehicleModel.findOneAndUpdate(criteria, dataToUpdate, options);
};

/**
 * function to get single vehicle 
 */
vehicleService.getVehicle = async (criteria) => {
  return await vehicleModel.findOne(criteria);
};

/**
 * function to get vehicles
 */
vehicleService.getVehicles = async (criteria, projection, options={}) => {
  return await vehicleModel.find(criteria, projection, options);
};

module.exports = vehicleService;