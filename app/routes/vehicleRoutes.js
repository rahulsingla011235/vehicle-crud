'use strict';

const { Joi } = require('../utils/joiUtils');
const { AVAILABLE_AUTHS } = require('../utils/constants');
const { userController, vehicleController } = require('../controllers');

let routes = [
	{
		method: 'POST',
		path: '/user/login',
		joiSchemaForSwagger: {
			body: {
				email: Joi.string().email().required().description('User\'s email Id.'),
				password: Joi.string().required().description('User\'s password.')
			},
			group: 'User',
			description: 'Route to login a user.',
			model: 'login'
		},
		handler: userController.loginUser
	},
	{
		method: 'POST',
		path: '/vehicle/add',
		joiSchemaForSwagger: {
			headers: {
				'authorization': Joi.string().required().description("User's JWT token.")
			},
			body: {
				make: Joi.string().lowercase().required().description('vehicle\'s make.'),
				model: Joi.string().lowercase().required().description('vehicle\'s model.'),
				year: Joi.string().lowercase().required().description('vehicle\'s manufacture year.')
			},
			group: 'Vehicle',
			description: 'Route to add a vehicle.',
			model: 'addVehicle'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: vehicleController.addVehicle
	},
	{
		method: 'PATCH',
		path: '/vehicle/update',
		joiSchemaForSwagger: {
			headers: {
				'authorization': Joi.string().required().description("User's JWT token.")
			},
			body: {
				id: Joi.string().required().description('vehicle\'s object Id.'),
				make: Joi.string().lowercase().optional().description('vehicle\'s make.'),
				model: Joi.string().lowercase().optional().description('vehicle\'s model.'),
				year: Joi.string().lowercase().optional().description('vehicle\'s year.')
			},
			group: 'Vehicle',
			description: 'Route to update vehicle.',
			model: 'updateVehicle'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: vehicleController.updateVehicle
	},
	{
		method: 'DELETE',
		path: '/vehicle/delete',
		joiSchemaForSwagger: {
			headers: {
				'authorization': Joi.string().required().description("User's JWT token.")
			},
			body: {
				id: Joi.string().required().description('Vehicle\'s object id.')
			},
			group: 'Vehicle',
			description: 'Route to delete a vehicle.',
			model: 'deleteVehicle'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: vehicleController.deleteVehicle
	},
	{
		method: 'GET',
		path: '/vehicle/getInfo',
		joiSchemaForSwagger: {
			headers: {
				'authorization': Joi.string().required().description("User's JWT token."),
			},
			query: {
				id: Joi.string().required().description('Vehicle\'s object Id.')
			},
			group: 'Vehicle',
			description: 'Route to get a vehicle details.',
			model: 'getVehicle'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: vehicleController.getVehicle
	},
	{
		method: 'GET',
		path: '/vehicle/getAll',
		joiSchemaForSwagger: {
			headers: {
				'authorization': Joi.string().required().description("User's JWT token.")
			},
			query: {
                pageNumber: Joi.number().min(1).optional().description('pageNumber'),
                limit: Joi.number().min(1).optional().description('Number of documents to be fetched'),
            },
			group: 'Vehicle',
			description: 'Route to get vehicle details.',
			model: 'getVehicles'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: vehicleController.getVehicles
	},
];

module.exports = routes;




